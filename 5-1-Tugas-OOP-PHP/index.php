<?php

abstract class Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        return "Harimau_1 sedang lari cepat";
    }
}

abstract class Fight{
    public $attackPower;
    public $defencePower;

    public function serang(){
        echo "Harimau_1 sedang menyerang elang_3";
    }

    public function diserang(){
        echo "Harimau_1 sedang diserang";
    }

}

class Elang extends Hewan {
    public function getInfoHewan(){

    }
}

class Harimau extends Fight{
    public function getInfoHewan(){
        
    }
}